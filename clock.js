let hours;
let minutes;
let seconds;

let cyan = "#2196F3";
let mgnt = "#E91E63";
let yllw = "#FFEB3B";

const canvas = document.getElementById('clock');
const ctx = canvas.getContext('2d');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
let cw = canvas.width;
let ch = canvas.height;


const updateTime = function(){
    let currentTime = new Date ();
    hours = currentTime.getHours();
    minutes = currentTime.getMinutes();
    seconds = currentTime.getSeconds();
    ms = currentTime.getMilliseconds();
    secMS = seconds + ms / 1000;
}
const degreesToRadians = function(degrees) {
    return (degrees * (Math.PI/180)) - (Math.PI / 2)
}

const drawClock = function(){
    updateTime();
    ctx.clearRect(0, 0, cw, ch);

    //draw background
    ctx.fillStyle = '#212121';
    ctx.fillRect(0, 0, cw, ch)

    //draw hours hand
    ctx.strokeStyle = cyan;
    ctx.lineWidth = 15;
    ctx.beginPath();
    let hourPos = 360 * (hours / 24);
    ctx.arc(cw / 2, ch / 2, 250, 1.5 * Math.PI, degreesToRadians(hourPos));
    ctx.stroke();

    //draw minutes hand
    ctx.strokeStyle = mgnt;
    ctx.lineWidth = 15;
    ctx.beginPath();
    let minutePos = 360 * (minutes / 60);
    ctx.arc(cw / 2, ch / 2, 225, 1.5 * Math.PI, degreesToRadians(minutePos));
    ctx.stroke();

    //draw seconds hand
    ctx.strokeStyle = yllw;
    ctx.lineWidth = 15;
    ctx.beginPath();
    let secondPos = 360 * (secMS / 60);
    ctx.arc(cw / 2, ch / 2, 200, 1.5 * Math.PI, degreesToRadians(secondPos));
    ctx.stroke();

    //draw text
    ctx.fillStyle = '#FFFDE7';
    ctx.font = "45px Arial";
    ctx.fillText(prependNumber(hours) + ':' + prependNumber(minutes) + ':' + prependNumber(seconds), cw/2, ch/2); 
}

function prependNumber(number){
    let numLength = number.toString().split('').length;
    if (numLength == 1){
        return "0" + number.toString();
    } else {
        return number;
    }
}
//resize canvas with window
window.addEventListener("resize", resizeCanvas, false);
function resizeCanvas(e){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    cw = canvas.width;
    ch = canvas.height;
}

drawClock();
setInterval(drawClock, 1);